import {
  createStore, combineReducers, compose
} from 'redux'

export const UPDATE_PROFILE = 'UPDATE_PROFILE'

const rootState = {
  profile: {
    name: '',
    description: ''
  },
}

const profileState = {
  name: '',
  description: ''
}

const profile = (state = profileState, action) => {

  switch (action.type) {
    case UPDATE_PROFILE:
      return {
        ...state,
        name: action.payload.name,
        description: action.payload.description,
      }
    default:
      return state
  }
}


const rootReducer = combineReducers({
  profile: profile
})

// const rootReducer = (state = rootState, action) => {
//   return {
//     ...state,
//     profile: profile(state.profile, action)
//   }
// }




export const store = createStore(rootReducer
  // window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
  //   // Prevents Redux DevTools from re-dispatching all previous actions.
  //   shouldHotReload: false
  // })()
  // redux-persist
)

export const updateProfile = (name,description) => ({
  type:'UPDATE_PROFILE', payload:{
    name, description
  }
})

export const profileSelector = state => state.profile