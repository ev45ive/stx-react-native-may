import React from 'react'
import MainNavigation from './navigation/MainNavigation';
import {
  StatusBar,
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView 
} from 'react-native'

import {AsyncStorage} from 'react-native'


// console.log(StatusBar.setBackgroundColor('red'))

import {Provider} from 'react-redux'
import {store, updateProfile} from './store'

export default class App extends React.Component {

  componentDidMount(){
    
    store.subscribe(()=>{
      const profile = store.getState().profile
      AsyncStorage.setItem('profile', JSON.stringify(profile))
    })
     AsyncStorage.getItem('profile').then( profile =>
        store.dispatch( updateProfile(profile.name, profile.description ))
     )
  }

  // <MyConsumer>
  //   {MyService => <View prop={MyServiec.prop}}
  // </MyConsumer>

  //HOC:  withConsumer(View)


  render() {
    return <Provider store={store}>
      <KeyboardAvoidingView style={styles.container} 
        enabled behavior="padding"> 
        <MainNavigation style={styles.main} />
      </KeyboardAvoidingView>
    </Provider>
  }
}


const styles = StyleSheet.create({
  container:{
    paddingTop:24,
    flex:1
  },
  main:{
    flex:1
  }
})