import {
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  createMaterialTopTabNavigator
} from "react-navigation";

import HotOrNot from './../components/hotornot/HotOrNot';
import Interests from './../components/Interests';
import UsersList from "../components/users/UsersList";
import Profile from './../components/profile/Profile';

// npm i --save react-navigation

const MainNavigation = createDrawerNavigator({
  Home: {
    screen: UsersList
  },
  Interests: {
    screen: Interests
  },
  HotOrNot: {
    screen: HotOrNot
  },
  Profile:Profile
}, {
  initialRouteName: 'Profile',
  tabBarOptions:{
    //  activeTintColor: '#e91e63',
    // labelStyle: {
    //   fontSize: 12,
    // },
    // style: {
    //   backgroundColor: 'blue',
    // },
  }
}); 


export default MainNavigation