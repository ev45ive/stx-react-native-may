import React from 'react';
import {StyleSheet,TouchableNativeFeedback,Alert, Image} from 'react-native'

export class TouchableLogo extends React.Component{

  render(){
    return (      
      <TouchableNativeFeedback style={styles.touchable}
            onPress={() => {
              // Alert.alert("You tapped the button!");
            }}>
          <Image source={{
            uri: 'https://facebook.github.io/react/logo-og.png',
            width:150, height:150
          }} />
        </TouchableNativeFeedback>  )
  }
}

const styles = StyleSheet.create({
  touchable:{ borderColor: "red" }
})