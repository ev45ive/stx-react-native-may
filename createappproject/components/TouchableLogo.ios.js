import React from 'react';
import {StyleSheet,TouchableHighlight,Alert, Image} from 'react-native'


const styles = StyleSheet.create({
  touchable:{ borderColor: "red" }
})

export class TouchableLogo extends React.Component{

  render(){
    return (      <TouchableHighlight style={styles.touchable}
            onPress={() => {
              Alert.alert("You tapped the button!");
            }}>
          <Image source={{
            uri: 'https://facebook.github.io/react/logo-og.png',
            width:50, height:50
          }} />
        </TouchableHighlight>  )
  }
}