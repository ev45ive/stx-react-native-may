import React from 'react'
import {View, Text, Image, StyleSheet, Animated, Easing, PanResponder} from 'react-native'
import {Card, Button, Icon} from 'react-native-elements'

class HotOrNot extends React.Component {

  static navigationOptions = {
    title: "Hot or Not!?" 
  }

  constructor(props) {
    super(props);
    this.state = {
      position: new Animated.ValueXY({x:0,y:0}) 
    };
 
    this.enterAnimation = Animated.spring(this.state.position,{
      toValue: {x:0,y:0},
      easing: Easing.ease,
      // useNativeDriver:true
    })

    this.responder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,

      onPanResponderMove:  (evt, gesture) => {
        if(gesture.dx > 100){
          console.log('I like her!')
        } else if( gesture.dx < -100){
          console.log('Next please ...')
        }

        Animated.event([null,{
            dx : this.state.position.x,
            dy : 0 //this.state.position.y
        }])(evt,gesture)
        
      },

      onPanResponderRelease: (/* evt, gestureState */) => {
        Animated.spring(this.state.position, {
          toValue: {x:0, y:0}
        }).start()
      },
    })
  }

  componentDidMount() {
    this.state.position.setValue({x:0,y:-500})
    this.enterAnimation.start()
  }

  action = () =>{
    this.state.position.setValue({x:0,y:-500})
    this.enterAnimation.start()
  }

  render() {
    return <View style={styles.container}>
      <Animated.View {...this.responder.panHandlers} style={{
        transform:[
        //   { translateY: this.state.position.y},
        //   { translateX: this.state.position.x},
          ...this.state.position.getTranslateTransform(),
          {rotate: this.state.position.x.interpolate({
            inputRange: [-150, 0, 150],
            outputRange: ['-50deg', '0deg', '50deg'] // 0 : 150, 0.5 : 75, 1 : 0
          })},
        ]
        // ...this.state.position.getLayout()
      }}>
        <Card
          title=''
          imageStyle={{height:400}}
          image={require('../../models/female/pexels-photo-206525.jpeg')}>
          <Text style={{padding:20}}>
          {this.props.navigation.state.params && this.props.navigation.state.params.id} 
          </Text>
        
        <Button
          icon={<Icon name='code' color='#ffffff' />}
          backgroundColor='#03A9F4'
          buttonStyle={{
            borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0
          }}
          onPress={this.action}
          title='VIEW NOW' />
        </Card>
      </Animated.View>
    </View>;
  }
}

export default HotOrNot;

const styles = StyleSheet.create({
  container:{
    paddingTop:40,
    flex:1
  },
  card:{
    
  }
})