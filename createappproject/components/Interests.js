import React from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet
} from 'react-native'
import InterestsList from './interests/InterestsList'
import InterestsEditor from './interests/InterestsEditor'

class Interests extends React.Component {

  static navigationOptions = {
    title: "Interests" 
  }


  constructor(props) {
    super(props);
    this.state = {
      interests:[
        '3D printing',
        'Acting',
        'Amateur radio',
        'Aquascaping',
        'Baking',
        'Baton twirling',
        'Board/tabletop games',
        'Book restoration',
        'Cabaret',
        'Magic',
        'Metalworking'
      ]
    }
    this.listRef = React.createRef()
  }
  addInterest = (text)=>{

    this.setState({
      interests: [ 
        ...this.state.interests,
        text
      ]
    }, () => {
      this.listRef.current.scrollToLast()
    })
  }
  
  render() {
    return <View style={styles.container}>
      <View style={styles.header}>
        <Text>Interests</Text>
      </View>
      
      <InterestsList 
      ref={this.listRef}
      data={this.state.interests} />
      
      <InterestsEditor onAddInterest={this.addInterest} />
    </View>;
  }
}

export default Interests;

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  header:{
    height:70, 
    padding:30, 
    backgroundColor:'steelblue'
  },
  list:{
    flex:1,
    backgroundColor:'lightgray'
  },
  editor:{
    height:70, paddingBottom:20//, flexDirection:'row'
  },
})