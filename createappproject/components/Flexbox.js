import React from 'react';
import { StyleSheet, Text, View, Image,Button,TextInput, Alert, TouchableHighlight, Platform} from 'react-native';
import {TouchableLogo} from './components/TouchableLogo'

export default class App extends React.Component {
  state = {
    text:''
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.top]}>
        <TouchableLogo></TouchableLogo>
        </View>
        <View style={{flex:3}}>    
          <View style={{padding: 10}}>
            <TextInput
              style={{height: 40}}
              placeholder="Type here!"
              multiline={true}
              numberOfLines = {4}
              maxLength = {40}
              onChangeText={(text) => this.setState({text})}
            />
            <Text style={{padding: 10, fontSize: parseInt(this.state.text.length)}}>
              {this.state.text}
            </Text>
          </View>
        </View>
        <View style={[styles.middle]}>
          <View style={styles.list}>
            <View style={styles.listItem}></View>
            <View style={styles.listItem}></View>
            <View style={styles.listItem}></View>
          </View>
        </View>
        <View style={[styles.bottom]}>
          <Button
            onPress={() => {
              Alert.alert("You tapped the button!");
            }}
            title="Press Me"
            color="green"
          />
        </View>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  top:{
    marginTop:20,
    // height:40,
    alignItems:'center',
    justifyContent:'center',
    flex:2,
    backgroundColor:'steelblue'
  },
  middle:{
    flex:8,
    backgroundColor:'powderblue'
  },
  bottom:{
    flex:1.5,
    // height:150,
    borderTopColor:'black',
    backgroundColor:'skyblue',
    flexDirection:'row',
    justifyContent:'space-evenly',
    paddingTop: 20
  },
  box:{
    width:50, height: 50,
    backgroundColor:'steelblue'
    // borderWidth:2,
    // borderColor:'black',
  },
  list:{
    margin:20,
    flex:1,
  },
  listItem:{
    backgroundColor:'#fff',
    flex:1,
    borderColor:'lightgray',
    borderWidth:1
  },
  container: {
    flex:1,
    alignItems:'stretch',
    borderWidth:2,
    borderColor:'red',
    height:400,
    backgroundColor: '#fff',
    // justifyContent:'space-between',
    // justifyContent:'space-around',
    justifyContent:'space-evenly',
    // alignItems:'center',
    // flexDirection:'row',
  },
  touchable:{
    borderWidth:2,
    ...Platform.select({
      ios: {
        borderColor: "red"
      },
      android: {
        borderColor: "blue"
      }
    })
  },
  background:{backgroundColor:'pink'}
});
