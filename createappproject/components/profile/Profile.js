import React from 'react'
import {View,Button,Text,TextInput, StyleSheet} from 'react-native'
import { updateProfile, profileSelector } from '../../store'


class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      name:'', description:''
     };    
  }

  // Map Props to local State
  static getDerivedStateFromProps(props){
    return {
      name: props.profile.name,
      description: props.profile.description,
    }
  }

  save = () => {
    this.props.onUpdate(this.state.name, this.state.description)
  }

  handleChange(field, text){
   this.setState({
     [field]:text
   })
  }

  render() {
    return (
      <View style={styles.form}>
        <View style={styles.field}>
          <TextInput 
          value={this.state.name}
          style={styles.input}
          onChangeText={(text)=>this.handleChange('name',text)}
          placeholder="Name"></TextInput>
        </View>
        <View style={styles.field}>
          <TextInput 
          style={styles.input}
          value={this.state.description}
          onChangeText={(text)=>this.handleChange('description',text)}
          multiline={true}
          numberOfLines={5}
          placeholder="Description"></TextInput>
        </View>
        <View style={styles.field}>
          <Button title="Save" onPress={this.save}></Button>
        </View>
      </View>
    );
  }
}

import {connect} from 'react-redux'

const mapStateToProps = state => ({
    profile: profileSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
  onUpdate: (name, description) => dispatch( updateProfile(name, description) )
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);

const styles = StyleSheet.create({
  form:{
    flex:1
  },
  field:{
    padding: 10,
  },
  input:{
    padding: 10,
  }
})