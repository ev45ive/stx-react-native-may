import React from 'react'
import {View,Text, Button, StyleSheet, ActivityIndicator, FlatList, RefreshControl} from 'react-native'
import { Avatar, ListItem } from 'react-native-elements'

// https://github.com/jshanson7/react-native-swipeable

class UsersList extends React.Component {

  static navigationOptions = {
    title: "Home",
  }

  constructor(props) {
    super(props);

    this.state = { 
      users: [],
    };
    
    this.viewabilityConfig = {
        waitForInteraction: true,
        viewAreaCoveragePercentThreshold: 95
    }
  }

  componentDidMount(){
    this.loadData()
  }
  
  loadData = () => {
    this.setState({
      loading:true,
    })
    fetch('https://api.github.com/users')
    .then( response => response.json() )
    .then( data =>{  
      this.setState({
        loading:false,
        users: data
      })
    }, err => console.error(err))
  }

  renderItem = ({item:user}) =>  <ListItem 
    onPress={()=>{
      this.props.navigation.navigate('HotOrNot',{
        id: user.login
      })
    }}
    title={user.login} 
    hideChevron={true}
    roundAvatar
    avatar={{  uri: user.avatar_url  }} />

  renderHeader = ({item:user}) =>  <Text style={styles.header}>Users </Text>
  renderFooter  = ({item:user}) =>  <Text style={styles.header}></Text>
  renderEmpty  = ({item:user}) =>  <Text>Nothing to do here ...</Text>

  render() {
    return (<View style={styles.container}>
      {/* {this.state.loading? <ActivityIndicator size="large" color="#0000ff" animating={true}  /> : null } */}
      <Button title="HotOrNot" onPress={()=>{
        this.props.navigation.navigate('HotOrNot')
      }} />
      <FlatList
        // refreshControl={<RefreshControl
        //   refreshing={this.state.loading}
        //   onRefresh={this.loadData}
        //   animating={true}
          
        // />}
        viewabilityConfig={this.viewabilityConfig}
        data={this.state.users} 
        renderItem={ this.renderItem}
        keyExtractor={ item => ''+item.id } 
        ListEmptyComponent={ this.renderEmpty}
        ListFooterComponent={this.renderFooter}
        // ListHeaderComponent={this.renderHeader}
        // ItemSeparatorComponent={()=><Text>Separator</Text>}
         />
    </View>);
  }
}


export default UsersList;

const styles = StyleSheet.create({
  header:{
    fontSize:20, backgroundColor:'teal', padding:10
  },
  container:{ 
    flex:1
  },
  userItem:{
    borderBottomWidth:1, borderColor:'black', padding:20
  }
})


function generateUsers(count = 500){
  return Array(count).fill(null)
  .map((x, index) => {
    return {
      key: 'c'+index,
      name: 'User ' + index
    }
  })
}