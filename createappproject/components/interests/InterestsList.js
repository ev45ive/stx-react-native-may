import React from 'react'
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  StyleSheet,InteractionManager,
} from 'react-native'

var {height, width} = Dimensions.get('window');

class InterestsList extends React.Component {

  constructor(props){
    super(props)
    this.listRef = React.createRef()
  }

  componentDidMount(){
    this.scrollToLast()
  }

  scrollToLast(){
    setTimeout(()=>{
      this.listRef.current.scrollToEnd()
    },10)
  }

  render() {
    return (
      <ScrollView ref={this.listRef}>
      {this.props.data.map( (interest, index) => 
        <TouchableOpacity key={interest}>
          <View style={styles.listItem}>
              <Text>{interest}</Text>
          </View>
        </TouchableOpacity>
      )}
    </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  listItem:{
    padding:20, 
    borderBottomWidth:1, 
    borderColor:'gray',
    backgroundColor:'#fff'
  },
})

export default InterestsList;