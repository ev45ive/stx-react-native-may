import React from 'react'
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
} from 'react-native'

class InterestsEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  
      text:''
    };
  }
  textChanged = (text) => {
    console.log(text)
    this.setState({
      text: text
    })
  }
  addInterest = () => {
    this.props.onAddInterest(this.state.text)
    this.setState({
      text:''
    })
  }
  render() {
    return (
      <View>
        <Text style={{marginLeft:150}}>{this.state.text}</Text>
      <TextInput style={styles.input}
          placeholder="Name your interest" 
          blurOnSubmit={ false }
          value={this.state.text}
          onChangeText={ this.textChanged }
          onSubmitEditing={ this.addInterest }
          ></TextInput>
      <Button style={styles.submit} title="Add Interest" onPress={this.addInterest}/>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  input:{
    padding:10
  },
  submit:{
  }
})


export default InterestsEditor;